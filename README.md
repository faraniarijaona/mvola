## Mvola for Prestashop
Mvola web payment module for Prestashop by [@faraniarijaona][1] .

*<u>Installation</u>*
1.	clone this repo
2.	zip it under `mvola.zip` 
3.	Go to Module Manager in the prestashop admin
4.	install module
*(alternative for step 1. and 2. : download directly the zip format from repo, then rename it from mvola-master.zip to mvola.zip)*
5. Configure it :
![Mvola module configuration](https://i.ibb.co/wytrvtS/mvola-conf.png)

As notification URL (you have to communicate it to telma), you have to provide them following ```{your_base_url}/module/mvola/validation``` or  ```{your_base_url}/module/mvola/notification```
 
 In order to get this information, you have to contact [telma][2] commercial service and you have to be a recognized organization with a legal notice.

*<u>want to integrate it to another technologie and CMS?</u>*
contact me by mail : [ffaraniarijaona@gmail.com](mailto:ffaraniarijaona@gmail.com?subject=mvola%20integration), 
Or [![Gitter](https://badges.gitter.im/faraniarijaona-mvola/community.svg)](https://gitter.im/faraniarijaona-mvola/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


Thanks!!!

## Reference
[https://shopin.mg](https://shopin.mg)

[1]:https://gitlab.com/faraniarijaona
[2]:https://telma.mg